package com.emiratesauction.views;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.emiratesauction.R;
import com.emiratesauction.base.BaseActivity;
import com.emiratesauction.base.BaseListener;
import com.emiratesauction.databinding.ActivityCarsMachineryBinding;
import com.emiratesauction.utils.Constants;
import com.emiratesauction.utils.MyApplication;
import com.emiratesauction.viewmodels.CarsMachineryViewModel;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import okhttp3.ResponseBody;

public class CarsMachineryActivity extends BaseActivity implements BaseListener, Observer {

    private ActivityCarsMachineryBinding activityBinding;
    private CarsMachineryViewModel carsMachineryViewModel;
    private CarsMachineryRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpLang();
        initDataBinding();
        setUpToolBar();
        setUpCarsList(activityBinding.rvCarsAndMachinery);
        setUpObservable(carsMachineryViewModel);
        setUpRefreshLayout();
    }

    private void setUpRefreshLayout() {
        activityBinding.swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        carsMachineryViewModel.getCarsAndMachinery();
                    }
                }
        );
    }

    private void initDataBinding(){
        activityBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_cars_machinery);
        carsMachineryViewModel = new CarsMachineryViewModel(this);
        activityBinding.setCarsMachineryViewModel(carsMachineryViewModel);
    }

    private void setUpToolBar() {
        setSupportActionBar(activityBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_cars_machinery));
        activityBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setUpCarsList(RecyclerView rvCars){
        activityBinding.swipeRefreshLayout.setRefreshing(true);
        adapter = new CarsMachineryRecyclerViewAdapter();
        adapter.setHasStableIds(true);
        adapter.setCarsList(activityBinding.getCarsMachineryViewModel().cars);
        rvCars.getItemAnimator().setChangeDuration(0);
        rvCars.setAdapter(adapter);
        rvCars.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemAnimator animator = rvCars.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
    }

    private void setUpObservable(Observable observable){
        observable.addObserver(this);
    }

    public static void setUpLang() {
        if(MyApplication.getLang().equals("")) {
            String deviceLang = Locale.getDefault().getLanguage();

            if(deviceLang.equals("")) {
                MyApplication.setLang(Constants.EN);
            }
            else if (!deviceLang.equalsIgnoreCase(Constants.AR) &&
                    !deviceLang.equalsIgnoreCase(Constants.EN)) {
                MyApplication.setLang(Constants.EN);
            }
            else {
                MyApplication.setLang(deviceLang);
            }
        }

        MyApplication.changeLang(MyApplication.getLang());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if(MyApplication.getLang().equals(Constants.EN))
            menu.getItem(0).setTitle(R.string.arabic);
        else
            menu.getItem(0).setTitle(R.string.english);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            if(MyApplication.getLang().equals(Constants.EN)){
                item.setTitle(R.string.english);
                MyApplication.setLang(Constants.AR);
            }
            else{
                item.setTitle(R.string.arabic);
                MyApplication.setLang(Constants.EN);
            }
            recreateActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void recreateActivity() {
        Intent intent = new Intent(this, CarsMachineryActivity.class);
        Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(this,
                android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle();
        ActivityCompat.startActivity(this, intent, bundle);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof CarsMachineryViewModel) {
            if (adapter != null)
                adapter.notifyDataSetChanged();
            else {
                setUpCarsList(activityBinding.rvCarsAndMachinery);
            }
            activityBinding.swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onConnectionFailed(String message) {
        super.onConnectionFailed(message);
        activityBinding.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onBusinessError(ResponseBody responseBody) {
        super.onBusinessError(responseBody);
        activityBinding.swipeRefreshLayout.setRefreshing(false);
    }
}
