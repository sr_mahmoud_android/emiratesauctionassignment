package com.emiratesauction.views;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.emiratesauction.R;
import com.emiratesauction.databinding.ItemCarsMachineryBinding;
import com.emiratesauction.models.Car;
import com.emiratesauction.viewmodels.ItemsCarsMachineryViewModel;

import java.util.List;

public class CarsMachineryRecyclerViewAdapter extends RecyclerView.Adapter<CarsMachineryRecyclerViewAdapter.CarsAdapterViewHolder> {

    private  List<Car> cars;

    public CarsMachineryRecyclerViewAdapter() {

    }

    public void setCarsList(List<Car> cars){
        this.cars = cars;
    }

    public CarsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCarsMachineryBinding itemAdsBinding = DataBindingUtil.inflate(LayoutInflater.from(
                parent.getContext()), R.layout.item_cars_machinery, parent, false);

        return new CarsAdapterViewHolder(itemAdsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarsAdapterViewHolder holder, int position) {
        holder.bindCar(cars.get(position));
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class CarsAdapterViewHolder extends RecyclerView.ViewHolder {

        private final ItemCarsMachineryBinding itemCarsMachineryBinding;
        public Context context;
        ValueAnimator colorAnimation;
        int colorFrom;
        int colorTo;

        public CarsAdapterViewHolder(ItemCarsMachineryBinding itemCarsMachineryBinding) {
            super(itemCarsMachineryBinding.itemLayout);
            this.itemCarsMachineryBinding = itemCarsMachineryBinding;
            this.context = itemCarsMachineryBinding.itemLayout.getContext();
            colorFrom = context.getResources().getColor(R.color.white);
            colorTo = context.getResources().getColor(R.color.gray);
        }

        void bindCar(final Car car){
            if(itemCarsMachineryBinding.getItemCarsViewModel() == null){
                itemCarsMachineryBinding.setItemCarsViewModel(
                        new ItemsCarsMachineryViewModel(car));
            }
            else{
                itemCarsMachineryBinding.getItemCarsViewModel().setCar(car);
            }

            // Under 5 mints, color of time label will be change to red till it get updated.
            if(car.getAuctionInfo().getEndDate() < 5 * 60)
                itemCarsMachineryBinding.tvTimeLeft.setTextColor(
                        ContextCompat.getColor(itemCarsMachineryBinding
                                .itemLayout.getContext(), R.color.red));
            else
                itemCarsMachineryBinding.tvTimeLeft.setTextColor(
                        ContextCompat.getColor(itemCarsMachineryBinding
                                .itemLayout.getContext(), R.color.primary_text));

            // Apply a flash of grey because the car price is updated.
            if(car.getAuctionInfo().isPriceUpdated()) {
                if (colorAnimation == null) {
                    colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
                    colorAnimation.setDuration(1000); // milliseconds
                    colorAnimation.setRepeatCount(4);
                    colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                        @Override
                        public void onAnimationUpdate(ValueAnimator animator) {
                            itemCarsMachineryBinding.llDataLayout.setBackgroundColor(
                                    (int) animator.getAnimatedValue());
                        }
                    });
                    colorAnimation.start();
                }
            }
        }
    }
}
