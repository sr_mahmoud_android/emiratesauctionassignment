package com.emiratesauction.viewmodels;

import android.os.CountDownTimer;

import com.emiratesauction.base.BaseListener;
import com.emiratesauction.base.BaseViewModel;
import com.emiratesauction.models.Car;
import com.emiratesauction.models.CarsOnlineResponseModel;
import com.emiratesauction.network.ApiFactory;
import com.emiratesauction.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsMachineryViewModel extends BaseViewModel {

    public List<Car> cars = new ArrayList<>();;
    public List<Integer> Ids = new ArrayList<>();;

    private BaseListener baseListener;
    private CountDownTimer timerTicks;

    public CarsMachineryViewModel(BaseListener baseListener){
        this.baseListener = baseListener;
        getCarsAndMachinery();
    }

    private void startTicks(final int refreshInterval) {
        if(timerTicks != null)
            timerTicks.cancel();

        timerTicks = new CountDownTimer(refreshInterval * 1000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        for (int i = 0; i < cars.size(); i++) {
                            cars.get(i).getAuctionInfo().setEndDate(
                                    cars.get(i).getAuctionInfo().getEndDate() - 1);

                            if(cars.get(i).getAuctionInfo().getEndDate() < 1){
                                cars.remove(i);
                                Ids.remove(i);
//                                setChanged();
//                                notifyObservers();
                                break;
                            }
                        }
                        setChanged();
                        notifyObservers();
                    }

                    public void onFinish() {
                        startTicks(refreshInterval);
                        getCarsAndMachinery();
                    }
                }.start();
    }

    public void getCarsAndMachinery() {
        Call<CarsOnlineResponseModel> responseCall = ApiFactory.create().getCarAndsMachinery();
        responseCall.enqueue(new Callback<CarsOnlineResponseModel>() {
            @Override
            public void onResponse(Call<CarsOnlineResponseModel> call,
                                   Response<CarsOnlineResponseModel> response) {

                if (response.code() == Constants.SUCCESS) {
                    updateCarsList(response.body().getCars());
                    startTicks(response.body().getRefreshInterval());
                    setChanged();
                    notifyObservers();
                }
                else if(response.code() == Constants.INTERNAL_SERVER_ERROR)
                    baseListener.onServerError();
                else
                    baseListener.onBusinessError(response.errorBody());
            }

            @Override
            public void onFailure(Call<CarsOnlineResponseModel> call, Throwable t) {
                baseListener.onConnectionFailed(null);
            }
        });
    }

    private void updateCarsList(List<Car> newList) {
        if(!cars.isEmpty()) {
            for (int i = 0; i < newList.size(); i++) {
                newList.get(i).getAuctionInfo().setPriceUpdated(isCarPriceUpdated(newList.get(i)));
            }
        }

        cars.clear();
        Ids.clear();
        for (int i = 0; i < newList.size(); i++) {
            cars.add(newList.get(i));
            Ids.add(newList.get(i).getCarID());
        }
    }

    private boolean isCarPriceUpdated(Car carNewData) {
        int id = carNewData.getCarID();

        if(Ids.contains(id)){
            int index = Ids.indexOf(id);

            if(cars.get(index).getAuctionInfo().getCurrentPrice().equals(
                    carNewData.getAuctionInfo().getCurrentPrice()))  // Car price not updated
                return false;
            else
                return true;
        }
        return true; // New car.
    }
}
