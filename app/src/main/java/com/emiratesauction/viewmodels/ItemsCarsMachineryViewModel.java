package com.emiratesauction.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.emiratesauction.R;
import com.emiratesauction.models.Car;
import com.emiratesauction.utils.Constants;
import com.emiratesauction.utils.MyApplication;
import com.squareup.picasso.Picasso;


public class ItemsCarsMachineryViewModel extends BaseObservable{

    @Bindable
    private Car car;

    public ItemsCarsMachineryViewModel(Car car) {
        this.car = car;
    }

    @Bindable
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
        notifyChange();
    }

    @Bindable
    public String getImage(){
        return car.getImage().replace("[w]", "200")
                .replace("[h]", "200");
    }

    @Bindable
    public String getTitle(){
        if(MyApplication.getLang().equals(Constants.EN))
            return car.getMakeEn() + " " + car.getModelEn() + " " + car.getYear();
        else
            return car.getMakeAr() + " " + car.getModelAr() + " " + car.getYear();
    }

    @Bindable
    public String getPrice(){
        return car.getAuctionInfo().getCurrentPrice() + " ";
    }

    @Bindable
    public String getCurrency(){
        if(MyApplication.getLang().equals(Constants.EN))
            return car.getAuctionInfo().getCurrencyEn();
        else
            return car.getAuctionInfo().getCurrencyAr();
    }

    @Bindable
    public String getLot(){
        return car.getAuctionInfo().getLot() + "";
    }

    @Bindable
    public String getBids(){
        return car.getAuctionInfo().getBids() + "";
    }

    @Bindable
    public String getTimeLeft(){
        return timeConvert(car.getAuctionInfo().getEndDate());
    }

    @Bindable
    public String getTickColor(){
        return "#ff0000";
    }

    public String timeConvert(int time) {
        int d = time/24/60/60;
        int h = time/60/60%24;
        int m = time/60%60;
        int s = time%60;
        String timeLeft = "";

        if(s > 0)
            timeLeft += s + "s";
        timeLeft = m + "m:" + timeLeft;
        if(h > 0)
            timeLeft = h + "h:" + timeLeft;
        if(d > 0)
            timeLeft = d + "d:" + timeLeft;

        return timeLeft;
    }

    @BindingAdapter("imageSmallUrl")
    public static void setImgSmall(ImageView imageView, String url){
        Picasso.get().load(url).error(R.drawable.place_holder).into(imageView);
    }
}
