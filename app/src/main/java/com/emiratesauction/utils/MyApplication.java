package com.emiratesauction.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

public class MyApplication extends Application {

    private static MyApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;
    }

    public static void changeLang(String lang) {

        setLang(lang);

        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(myLocale);
        } else {
            config.locale = myLocale;
        }
        mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());
    }

    public static String getLang() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        return pref.getString(Constants.PREF_LANG, "");
    }

    public static void setLang(String lang) {
        SharedPreferences mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.PREF_LANG, lang);
        editor.commit();
    }
}
