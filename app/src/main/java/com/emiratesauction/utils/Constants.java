package com.emiratesauction.utils;

public class Constants {

    public static Integer SUCCESS = 200;
    public static Integer INTERNAL_SERVER_ERROR = 500;

    public static final String PREF_LANG = "PREF_LANG";

    public static String EN = "en";
    public static String AR = "ar";
}
