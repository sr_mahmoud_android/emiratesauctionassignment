package com.emiratesauction.network;

import com.emiratesauction.models.CarsOnlineResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitInterface {

    // Get cars and machinery
    @GET(ApiFactory.GET_CARS_AND_MACHINERY_API)
    Call<CarsOnlineResponseModel> getCarAndsMachinery();
}
