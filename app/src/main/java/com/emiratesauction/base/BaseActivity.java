package com.emiratesauction.base;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.emiratesauction.R;

import okhttp3.ResponseBody;

public class BaseActivity extends AppCompatActivity implements BaseListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onConnectionFailed(String message) {
        Snackbar.make(findViewById(R.id.main_layout), getString(R.string.internet_connection_error),
                Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    @Override
    public void onServerError() {
        Snackbar.make(findViewById(R.id.main_layout), getString(R.string.server_error),
                Snackbar.LENGTH_SHORT).setAction("Action", null).show();

    }

    @Override
    public void onBusinessError(ResponseBody responseBody) {

    }
}
