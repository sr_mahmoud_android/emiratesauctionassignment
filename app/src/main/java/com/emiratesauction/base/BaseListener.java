package com.emiratesauction.base;

import okhttp3.ResponseBody;

public interface BaseListener {
    void onConnectionFailed(String message);
    void onServerError();
    void onBusinessError(ResponseBody responseBody);
}
